package com;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
  // Find your Account Sid and Token at twilio.com/user/account
  public static final String ACCOUNT_SID = "AC3af626ed6663e59f42e7d24f5d40e09b";
  public static final String AUTH_TOKEN = "1789ed533acca12177f90a05de79d201";
  public static void send(String str) {
    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    Message message = Message
        .creator(new PhoneNumber("+380939287186"),
            /*my phone number*/
            new PhoneNumber("+15869304152"), str) .create(); /*attached to me number*/
  }
}
